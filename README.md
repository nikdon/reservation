# Movie Ticket Reservation System

A ticket reservation system for movies with http/json based interactions.

## Quickstart

Build a docker image and start service:

```shell
$ sbt docker:publishLocal
$ docker run -d -p 8080:8080 blast
$ curl -X GET http://$(docker-machine ip):8080/api/reservation/test/test
```

## API

### Version

`/api/version`

| Endpoint    | Method | Description                                            | 
|-------------|--------|--------------------------------------------------------| 
| api/version | GET    | Returns a version of the app                           | 

### Reservation

#### Register a movie

| Endpoint        | Method |  
|-----------------|--------| 
| api/reservation | POST   | 

System accepts a json with following structure to be able to register a movie before any reservation can happen.

    {
        "imdbId": "tt0111161",
        "availableSeats": 100,
        "screenId": "screen_123456"
    }
         
Where: 
* `imdbId` is IMDB movie identifier
* `availableSeats` the total seats available for this movie
* `screenId` is an externally managed identifier of information when and where the movie is screened.

Returns:

    {
      "Done": {}
    }

#### Reserve a seat at the movie

| Endpoint        | Method |  
|-----------------|--------| 
| api/reservation | POST   | 

System allows to reserve a seat for the movie. It should consume a request with following json to reserve a single seat for the movie.
    
    {
        "imdbId": "tt0111161",
        "screenId": "screen_123456"
    }    
 
Where:
 
* `imdbId` is IMDB movie identifier
* `screenId` is an externally managed identifier of information when and where the movie is screened.

Returns:

    {
        "imdbId": "test",
        "screenId": "test",
        "seat": 1
    }

or

    {
        "message": "There is no registered movie test:test"
    }



#### Retrieve information about the movie 

| Endpoint                               | Method | Description                                                      | 
|----------------------------------------|--------|------------------------------------------------------------------| 
| api/reservation/`imdbId`/`screenId`    | GET    | Returns a status of the reservation with `imdbId` and `screenId` |


System should allow to see how many seats are reserved for a movie as well as the movie title.
It consumes a request with `imdbId` and `screenId` and return following response:

    {
        "imdbId": "tt0111161",
        "screenId": "screen_123456",
        "movieTitle": "The Shawshank Redemption",
        "availableSeats": 100,
        "reservedSeats": 50
    }   

Where:
* `imdbId` is IMDB movie identifier
* `screenId` is an externally managed identifier of information when and where the movie is screened.
* `movieTitle` is the title of the movie
* `availableSeats` the total seats available for this movie
* `reservedSeats` the total number of reserved seats for a movie and screen.

Returns either:

    {
        "title": "This is a mock title",
        "reservationState": {
            "imdbId": "test",
            "screenId": "test",
            "availableSeats": 9,
            "reservedSeats": 1
        }
    }
    
or

    {
        "message": "Reservation test:test not found"
    }