package com.github.nikdon.reserveme.reservation.api

import akka.Done
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.github.nikdon.reserveme.common.api.RoutesSupport
import com.github.nikdon.reserveme.reservation.domain.Command.{RegisterMovie, ReserveSeat}
import com.github.nikdon.reserveme.reservation.domain.{MovieReservationError, MovieSession, MovieTicket}
import com.github.nikdon.reserveme.reservation.imdb.impl.ImdbServiceAlgFutureMockImpl
import com.github.nikdon.reserveme.reservation.repository.impl.AkkaPersistenceSessionRepository
import com.github.nikdon.reserveme.reservation.service.MovieReservationService
import io.circe.generic.auto._
import io.circe.syntax._
import org.scalatest.{FlatSpec, Matchers}
import cats.implicits._

import scala.concurrent.{ExecutionContext, Future}

class MovieReservationRoutesTest extends FlatSpec with Matchers with ScalatestRouteTest with RoutesSupport {

  val routes = new MovieReservationRoutes {
    override val movieReservationService: MovieReservationService[Future] = {
      implicit val ec: ExecutionContext = system.dispatcher
      new MovieReservationService[Future](
        new AkkaPersistenceSessionRepository(system),
        new ImdbServiceAlgFutureMockImpl
      )
    }
  }.movieReservationRoutes

  val imdbId         = "imdbIdTest"
  val screenId       = "screenIdTest"
  val availableSeats = 5

  it should "return an error for unregistered movie reservation" in {
    Get(s"/reservation/invalid/$screenId") ~> routes ~> check {
      responseAs[MovieReservationError] shouldEqual MovieReservationError(s"Reservation invalid:$screenId not found")
    }
  }

  it should "return a 200 response on register movie command" in {
    Post("/reservation", RegisterMovie(imdbId, screenId, availableSeats).asJson) ~> routes ~> check {
      responseAs[Done.type] shouldBe Done
    }
  }

  it should "return a 200 response on seat reservation" in {
    Post("/reservation", ReserveSeat(imdbId, screenId).asJson) ~> routes ~> check {
      responseAs[MovieTicket] shouldEqual MovieTicket(imdbId, screenId, _: Int)
    }
  }

  it should "return an error for unregistered movie seat reservation" in {
    Post(s"/reservation", ReserveSeat("invalid", screenId).asJson) ~> routes ~> check {
      responseAs[MovieReservationError] shouldEqual MovieReservationError(s"There is no registered movie invalid:$screenId")
    }
  }

  it should "return a valid reservation status" in {
    Get(s"/reservation/$imdbId/$screenId") ~> routes ~> check {
      responseAs[MovieSession] shouldBe a[MovieSession]
    }
  }
}
