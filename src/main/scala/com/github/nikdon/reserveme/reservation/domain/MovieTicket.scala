package com.github.nikdon.reserveme.reservation.domain

final case class MovieTicket(imdbId: String, screenId: String, seat: Int)
