package com.github.nikdon.reserveme.reservation.repository.impl

import java.util.concurrent.ConcurrentHashMap

import akka.Done
import akka.actor.{ActorRef, ActorSystem, ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider}
import akka.pattern.ask
import akka.persistence.query.journal.leveldb.scaladsl.LeveldbReadJournal
import akka.persistence.query.{EventEnvelope, PersistenceQuery}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.util.Timeout
import com.github.nikdon.reserveme.reservation.domain.Command.{RegisterMovie, ReserveSeat}
import com.github.nikdon.reserveme.reservation.domain.Event.{MovieRegistered, SeatNotReserved, SeatReserved}
import com.github.nikdon.reserveme.reservation.domain.{MovieReservationError, MovieTicket, ReservationState}
import com.github.nikdon.reserveme.reservation.repository.SessionRepositoryAlg
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.Future
import scala.concurrent.duration._

class AkkaPersistenceSessionRepository(system: ActorSystem)
    extends Extension
    with SessionRepositoryAlg[Future]
    with StrictLogging {

  implicit val timeout: Timeout       = 5.seconds
  implicit val ec                     = system.dispatcher
  implicit val mat: ActorMaterializer = ActorMaterializer()(system)

  private[this] val movieReg = new ConcurrentHashMap[String, ActorRef]()

  val queries: LeveldbReadJournal =
    PersistenceQuery(system).readJournalFor[LeveldbReadJournal](LeveldbReadJournal.Identifier)

  override def registerMovie(imdbId: String, screenId: String, availableSeats: Int): Future[Done] = {
    val reservationHandler = movieReg.getOrDefault(
      imdbId + screenId,
      system.actorOf(MovieReservationActor.props(imdbId, screenId, availableSeats)))
    movieReg.putIfAbsent(imdbId + screenId, reservationHandler)
    (reservationHandler ? RegisterMovie(imdbId, screenId, availableSeats))
      .mapTo[MovieRegistered]
      .map(_ => Done)
  }

  override def reserveSeat(imdbId: String, screenId: String): Future[Either[MovieReservationError, MovieTicket]] = {
    Option(movieReg.get(imdbId + screenId)) match {
      case Some(ref) =>
        (ref ? ReserveSeat(imdbId, screenId)).map {
          case SeatReserved(state)       => Right(MovieTicket(state.imdbId, state.screenId, state.reservedSeats))
          case SeatNotReserved(cause, _) => Left(MovieReservationError(cause))
        }

      case None =>
        Future.successful(Left(MovieReservationError(s"There is no registered movie $imdbId:$screenId")))
    }
  }

  override def state(imdbId: String, screenId: String): Future[Either[MovieReservationError, ReservationState]] = {
    queries
      .currentEventsByPersistenceId(persistenceId = imdbId + screenId)
      .collect {
        case EventEnvelope(_, _, _, event: MovieRegistered) => event.state
        case EventEnvelope(_, _, _, event: SeatReserved)    => event.state
      }
      .runWith(Sink.lastOption.mapMaterializedValue(_.map {
        _.fold[Either[MovieReservationError, ReservationState]](
          Left(MovieReservationError(s"Reservation $imdbId:$screenId not found")))(Right.apply)
      }))
  }
}

object AkkaPersistenceSessionRepository
    extends ExtensionId[AkkaPersistenceSessionRepository]
    with ExtensionIdProvider {
  override def lookup = AkkaPersistenceSessionRepository

  override def createExtension(system: ExtendedActorSystem) = new AkkaPersistenceSessionRepository(system)
}
