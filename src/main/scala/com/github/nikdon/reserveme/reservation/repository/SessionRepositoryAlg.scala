package com.github.nikdon.reserveme.reservation.repository

import akka.Done
import com.github.nikdon.reserveme.reservation.domain.{MovieReservationError, MovieTicket, ReservationState}

/** A movie session repository trait */
trait SessionRepositoryAlg[F[_]] {

  /**
    * Register a movie for reservations
    *
    * @param imdbId         The IMDb movie identifier
    * @param screenId       An externally managed identifier of information when and where the movie is screened
    * @param availableSeats Number of available seats
    */
  def registerMovie(imdbId: String, screenId: String, availableSeats: Int): F[Done]

  /**
    * Reserve a seat for the movie
    *
    * @param imdbId   The IMDb movie identifier
    * @param screenId An externally managed identifier of information when and where the movie is screened
    * @return         A `MovieTicket` or `MovieReservationError`
    */
  def reserveSeat(imdbId: String, screenId: String): F[Either[MovieReservationError, MovieTicket]]

  /**
    * Retrieves information about the movie session
    *
    * @param imdbId   The IMDb movie identifier
    * @param screenId An externally managed identifier of information when and where the movie is screened
    * @return         A `ReservationState` or `MovieReservationError`
    */
  def state(imdbId: String, screenId: String): F[Either[MovieReservationError, ReservationState]]
}
