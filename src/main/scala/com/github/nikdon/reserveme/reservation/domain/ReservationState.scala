package com.github.nikdon.reserveme.reservation.domain

import com.github.nikdon.reserveme.reservation.domain.Command.ReserveSeat

/**
  * Movie reservation state holder
  *
  * @param imdbId         The IMDb movie identifier
  * @param screenId       An externally managed identifier of information when and where the movie is screened
  * @param availableSeats Number of available seats
  * @param reservedSeats  Number of reserved seats
  */
final case class ReservationState(imdbId: String, screenId: String, availableSeats: Int, reservedSeats: Int = 0) {

  /** Update current state with `ReserveSeat` event */
  def updated(evt: ReserveSeat): Option[ReservationState] =
    if (availableSeats > 0) Some(copy(availableSeats = availableSeats - 1, reservedSeats = reservedSeats + 1))
    else None
}

final case class MovieSession(title: String, reservationState: ReservationState)
