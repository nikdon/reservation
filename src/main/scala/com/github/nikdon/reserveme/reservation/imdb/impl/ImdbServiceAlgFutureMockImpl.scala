package com.github.nikdon.reserveme.reservation.imdb.impl

import com.github.nikdon.reserveme.reservation.domain.MovieReservationError
import com.github.nikdon.reserveme.reservation.imdb.ImdbServiceAlg

import scala.concurrent.Future

class ImdbServiceAlgFutureMockImpl extends ImdbServiceAlg[Future] {
  override def title(imdbId: String): Future[Either[MovieReservationError, String]] =
    Future.successful(Right("This is a mock title"))
}
