package com.github.nikdon.reserveme.reservation.service

import akka.Done
import cats.Monad
import cats.data.EitherT
import cats.implicits._
import com.github.nikdon.reserveme.reservation.domain.{MovieReservationError, MovieSession, MovieTicket}
import com.github.nikdon.reserveme.reservation.imdb.ImdbServiceAlg
import com.github.nikdon.reserveme.reservation.repository.SessionRepositoryAlg

/**
  * Movie reservation service on top of `SessionRepositoryAlg` and `ImdbService`
  *
  * @param sessionRepository An implementation of the repository for storing movie sessions
  * @param imdbService       An implementation of the service that works with IMDb API
  */
class MovieReservationService[F[_]: Monad](sessionRepository: SessionRepositoryAlg[F], imdbService: ImdbServiceAlg[F]) {

  /**
    * Register a movie for reservations
    *
    * @param imdbId         The IMDb movie identifier
    * @param screenId       An externally managed identifier of information when and where the movie is screened
    * @param availableSeats Number of available seats
    * @return
    */
  def registerMovie(imdbId: String, screenId: String, availableSeats: Int): F[Done] =
    sessionRepository.registerMovie(imdbId, screenId, availableSeats)

  /**
    * Make a reservation for a movie
    *
    * @param imdbId   The IMDb movie identifier
    * @param screenId An externally managed identifier of information when and where the movie is screened
    * @return
    */
  def reserveSeat(imdbId: String, screenId: String): F[Either[MovieReservationError, MovieTicket]] =
    sessionRepository.reserveSeat(imdbId, screenId)

  /**
    * Return current state of the movie reservation
    *
    * @param imdbId   The IMDb movie identifier
    * @param screenId An externally managed identifier of information when and where the movie is screened
    * @return
    */
  def state(imdbId: String, screenId: String): F[Either[MovieReservationError, MovieSession]] = {
    (EitherT(sessionRepository.state(imdbId, screenId)) |@| EitherT(imdbService.title(imdbId))).map {
      case (reservationState, title) => MovieSession(title, reservationState)
    }.value
  }
}
