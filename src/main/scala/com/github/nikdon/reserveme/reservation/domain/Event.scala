package com.github.nikdon.reserveme.reservation.domain

sealed trait Event
object Event {
  final case class MovieRegistered(state: ReservationState)                extends Event
  final case class SeatReserved(state: ReservationState)                   extends Event
  final case class SeatNotReserved(cause: String, state: ReservationState) extends Event
  final case class StateSent(state: ReservationState)                      extends Event
}
