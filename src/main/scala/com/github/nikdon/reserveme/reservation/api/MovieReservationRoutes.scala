package com.github.nikdon.reserveme.reservation.api

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.github.nikdon.reserveme.common.api.RoutesSupport
import com.github.nikdon.reserveme.reservation.domain.Command.{RegisterMovie, ReserveSeat}
import com.github.nikdon.reserveme.reservation.service.MovieReservationService
import io.circe.generic.auto._

import scala.concurrent.Future

trait MovieReservationRoutes extends RoutesSupport {
  def movieReservationService: MovieReservationService[Future]

  val movieReservationRoutes: Route = pathPrefix("reservation") {
    post {
      entity(as[RegisterMovie]) { rm =>
        complete(movieReservationService.registerMovie(rm.imdbId, rm.screenId, rm.availableSeats))
      } ~
        entity(as[ReserveSeat]) { rs =>
          complete(movieReservationService.reserveSeat(rs.imdbId, rs.screenId))
        }
    } ~
      path(Segment / Segment) {
        case (imdbId, screenId) =>
          complete(movieReservationService.state(imdbId, screenId))
      }
  }
}
