package com.github.nikdon.reserveme.reservation.imdb

import com.github.nikdon.reserveme.reservation.domain.MovieReservationError

/** An algebra that describes behaviour of the service to retrive IMDb information */
trait ImdbServiceAlg[F[_]] {

  /** Returns a title of the movie */
  def title(imdbId: String): F[Either[MovieReservationError, String]]
}
