package com.github.nikdon.reserveme.reservation.domain

sealed trait Command
object Command {
  final case class RegisterMovie(imdbId: String, screenId: String, availableSeats: Int) extends Command
  final case class ReserveSeat(imdbId: String, screenId: String)                        extends Command
  final case class SendState(imdbId: String, screenId: String)                          extends Command
}
