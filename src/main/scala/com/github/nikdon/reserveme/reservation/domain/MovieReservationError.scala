package com.github.nikdon.reserveme.reservation.domain

final case class MovieReservationError(message: String)
