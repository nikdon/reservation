package com.github.nikdon.reserveme.reservation.repository.impl

import akka.actor.{ActorLogging, Props}
import akka.persistence.{PersistentActor, RecoveryCompleted}
import com.github.nikdon.reserveme.reservation.domain.Command._
import com.github.nikdon.reserveme.reservation.domain.Event._
import com.github.nikdon.reserveme.reservation.domain.ReservationState

object MovieReservationActor {

  /**
    * Props of the actor that holds information about movie session reservation state
    *
    * @param imdbId         The IMDb movie identifier
    * @param screenId       An externally managed identifier of information when and where the movie is screened
    * @param availableSeats The total seats available for this movie
    * @return
    */
  def props(imdbId: String, screenId: String, availableSeats: Int): Props =
    Props(new MovieReservationActor(imdbId, screenId, availableSeats))
}

class MovieReservationActor(imdbId: String, screenId: String, availableSeats: Int)
    extends PersistentActor
    with ActorLogging {

  var state: ReservationState = ReservationState(imdbId, screenId, availableSeats)

  override def persistenceId: String = imdbId + screenId

  override def receiveRecover: Receive = {
    case e @ MovieRegistered(recoveredState) => state = recoveredState
    case e @ SeatReserved(recoveredState)    => state = recoveredState
    case RecoveryCompleted                   => log.info(s"Recovery completed: $state")
    case unexpected                          => log.error(s"Unexpected message: $unexpected")
  }

  override def receiveCommand: Receive = {
    case RegisterMovie(_, _, _) =>
      persist(MovieRegistered(state)) { e =>
        sender ! e
      }

    case rs @ ReserveSeat(_, _) =>
      state.updated(rs) match {
        case Some(newState) =>
          persist(SeatReserved(newState)) { e =>
            state = e.state
            sender ! SeatReserved(state)
          }
        case None =>
          sender ! SeatNotReserved("No seats available", state)
      }

    case SendState(_, _) => sender ! StateSent(state)

    case unexpected => log.error(s"Unexpected message: $unexpected")
  }
}
