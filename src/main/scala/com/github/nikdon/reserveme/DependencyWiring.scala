package com.github.nikdon.reserveme

import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher
import com.github.nikdon.reserveme.reservation.imdb.impl.ImdbServiceAlgFutureMockImpl
import com.github.nikdon.reserveme.reservation.repository.impl.AkkaPersistenceSessionRepository
import com.github.nikdon.reserveme.reservation.service.MovieReservationService
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import cats.implicits._

import scala.concurrent.Future

trait DependencyWiring extends StrictLogging {
  def system: ActorSystem

  lazy val config = new ServerConfig {
    override def rootConfig: Config = ConfigFactory.load()
  }

  lazy val serviceExecutionContext: MessageDispatcher = system.dispatchers.lookup("service-dispatcher")

  lazy val movieReservationService: MovieReservationService[Future] = {
    implicit val ec = serviceExecutionContext
    new MovieReservationService[Future](AkkaPersistenceSessionRepository(system), new ImdbServiceAlgFutureMockImpl)
  }
}
