package com.github.nikdon.reserveme.common.api

import akka.http.scaladsl.server.Directives._
import com.github.nikdon.reserveme.common.api.`X-Content-Type-Options`.`nosniff`
import com.github.nikdon.reserveme.common.api.`X-Frame-Options`.`DENY`
import com.github.nikdon.reserveme.common.api.`X-XSS-Protection`.`1; mode=block`
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport

trait RoutesSupport extends ErrorAccumulatingCirceSupport

trait SecuritySupport {
  val addSecurityHeaders = respondWithHeaders(
    `X-Frame-Options`(`DENY`),
    `X-Content-Type-Options`(`nosniff`),
    `X-XSS-Protection`(`1; mode=block`)
  )
}
