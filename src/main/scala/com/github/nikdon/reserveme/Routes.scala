package com.github.nikdon.reserveme

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import com.github.nikdon.reserveme.common.api.RoutesRequestWrapper
import com.github.nikdon.reserveme.reservation.api.MovieReservationRoutes
import com.github.nikdon.reserveme.version.VersionRoutes

trait Routes extends RoutesRequestWrapper with MovieReservationRoutes with VersionRoutes {

  def system: ActorSystem
  def config: ServerConfig

  lazy val routes = requestWrapper {
    pathPrefix("api") {
      concat(
        versionRoutes,
        movieReservationRoutes
      )
    }
  }
}
