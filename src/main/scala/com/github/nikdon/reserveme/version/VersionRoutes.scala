package com.github.nikdon.reserveme.version

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import com.github.nikdon.reserveme.common.api.RoutesSupport
import com.github.nikdon.reserveme.version.BuildInfo._
import io.circe.generic.auto._

final case class Version(build: String, date: String)

trait VersionRoutes extends RoutesSupport {

  val versionRoutes = pathPrefix("version") {
    pathEndOrSingleSlash {
      getVersion
    }
  }

  def getVersion: Route =
    complete {
      Version(buildSha.take(6), buildDate)
    }
}
