import java.text.SimpleDateFormat
import java.util.Date

import scala.util.Try

val slf4jVersion        = "1.7.21"
val logBackVersion      = "1.1.7"
val scalaLoggingVersion = "3.5.0"

val circeVersion = "0.8.0"

val akkaVersion     = "2.5.1"
val akkaHttpVersion = "10.0.6"

val cats = "org.typelevel" %% "cats" % "0.9.0"

val slf4jApi       = "org.slf4j" % "slf4j-api" % slf4jVersion
val logBackClassic = "ch.qos.logback" % "logback-classic" % logBackVersion
val scalaLogging   = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
val loggingStack   = Seq(slf4jApi, logBackClassic, scalaLogging)

val typesafeConfig = "com.typesafe" % "config" % "1.3.1"

val circeCore     = "io.circe" %% "circe-core" % circeVersion
val circeGeneric  = "io.circe" %% "circe-generic" % circeVersion
val circeJawn     = "io.circe" %% "circe-jawn" % circeVersion
val akkaHttpCirce = "de.heikoseeberger" %% "akka-http-circe" % "1.16.0"
val circeStack    = Seq(circeCore, circeGeneric, circeJawn, akkaHttpCirce)

val scalatest        = "org.scalatest" %% "scalatest" % "3.0.1" % "test"
val unitTestingStack = Seq(scalatest)

val akka            = "com.typesafe.akka" %% "akka-actor"        % akkaVersion
val akkaStreams     = "com.typesafe.akka" %% "akka-stream"       % akkaVersion
val akkaHttp        = "com.typesafe.akka" %% "akka-http"         % akkaHttpVersion
val akkaHttpTestkit = "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test"

val akkaStack = Seq(akka, akkaStreams, akkaHttp, akkaHttpTestkit)

val akkaPersistence      = "com.typesafe.akka" %% "akka-persistence" % akkaVersion
val akkaPersistenceQuery = "com.typesafe.akka" %% "akka-persistence-query" % akkaVersion
val leveldb              = "org.iq80.leveldb" % "leveldb" % "0.7"
val leveldbjni           = "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"
val akkaPersistenceStack = Seq(akkaPersistence, akkaPersistenceQuery, leveldb, leveldbjni)

val commonDependencies = unitTestingStack ++ loggingStack

lazy val commonSettings = Seq(
  name := "reservation",
  version := "0.1.0",
  scalaVersion := "2.12.2",
  crossVersion := CrossVersion.binary,
  scalacOptions ++= Seq("-unchecked", "-deprecation"),
  libraryDependencies ++= commonDependencies
)

lazy val root = (project in file("."))
  .enablePlugins(BuildInfoPlugin)
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Seq(cats) ++ loggingStack ++ circeStack ++ unitTestingStack ++ akkaStack ++ akkaPersistenceStack,
    buildInfoPackage := "com.github.nikdon.reserveme.version",
    buildInfoObject := "BuildInfo",
    buildInfoKeys := Seq[BuildInfoKey](
      BuildInfoKey.action("buildDate")(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())),
      // if the build is done outside of a git repository, we still want it to succeed
      BuildInfoKey.action("buildSha")(Try(Process("git rev-parse HEAD").!!.stripLineEnd).getOrElse("?"))
    ),
    fork := true
  )
  .enablePlugins(JavaServerAppPackaging)
